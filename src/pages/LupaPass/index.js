import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { setUserSession } from "../../utils/Common";

export class LupaPass extends Component {
  state = {
    formCustomer: {
      email: "",
    },
    redirect: null,
  };

  handleFormChange = (e) => {
    let formCustomerNew = { ...this.state.formCustomer };
    formCustomerNew[e.target.name] = e.target.value;
    this.setState({
      formCustomer: formCustomerNew,
    });
  };

  handleSubmit = () => {
    if (this.state.redirect) {
        return <Redirect to={this.state.redirect} />;
      }
    axios
      .post(
        process.env.REACT_APP_SERVER_URL + "login/forgot",
        this.state.formCustomer
      )
      .then((res) => {
        if (res.data.status == "200") {
            alert(res.data.msg)
            this.setState({ redirect: "/kode-otp" });
        } else {
          alert(res.data.status);
        }
      });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <Container fluid>
        <Row>
          <Col>
            <h4 className="text-center mt-5">Lupa Password</h4>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                type="email"
                placeholder="Ketikan Email Anda"
                name="email"
              />
            </Form.Group>

            <Button onClick={this.handleSubmit} variant="primary">
              Submit
            </Button>
            <br />
            <Link to="/login">Kembali ke halaman login</Link><br/>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

export default LupaPass;
