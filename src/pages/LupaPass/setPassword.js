import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { setUserSession } from "../../utils/Common";

export class setPassword extends Component {
  state = {
    formCustomer: {
      password: "",
      password2: "",
      user_id:localStorage.getItem('user_id')
    },
    redirect: null,
  };

  handleFormChange = (e) => {
    let formCustomerNew = { ...this.state.formCustomer };
    formCustomerNew[e.target.name] = e.target.value;
    this.setState({
      formCustomer: formCustomerNew,
    });
  };

  handleSubmit = () => {
    if (this.state.redirect) {
        localStorage.removeItem('user_id')
        return <Redirect to={this.state.redirect} />;
      }
    axios
      .post(
        process.env.REACT_APP_SERVER_URL + "login/reset_password",
        this.state.formCustomer
      )
      .then((res) => {
        if (res.data.status == "200") {
            alert(res.data.msg)
            this.setState({ redirect: "/login" });
        } else {
          alert(res.data.status);
        }
      });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <Container fluid>
        <Row>
          <Col>
            <h4 className="text-center mt-5">Lupa Password</h4>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                type="password"
                placeholder="Ketikan Password Anda"
                name="password"
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Ketik Ulang Password</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                type="password"
                placeholder="Ketikan Lagi Password Anda"
                name="password2"
              />
            </Form.Group>

            <Button onClick={this.handleSubmit} variant="primary">
              Submit
            </Button>
            <br />
            <Link to="/login">Kembali ke halaman login</Link><br/>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

export default setPassword;
