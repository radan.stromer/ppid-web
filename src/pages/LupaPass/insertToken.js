import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { setUserSession } from "../../utils/Common";

export class InsertToken extends Component {
  state = {
    formCustomer: {
        otp: "",
    },
    redirect: null,
  };

  handleFormChange = (e) => {
    let formCustomerNew = { ...this.state.formCustomer };
    formCustomerNew[e.target.name] = e.target.value;
    this.setState({
      formCustomer: formCustomerNew,
    });
  };

  handleSubmit = () => {
    axios
      .post(
        process.env.REACT_APP_SERVER_URL + "login/submit_otp",
        this.state.formCustomer
      )
      .then(async (res) => {
        if (res.data.status == "200") {
            await localStorage.setItem('user_id',res.data.user_id)
            this.setState({ redirect: "/ubah-password" });
        } else {
          alert(res.data.status);
        }
      })
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <Container fluid>
        <Row>
          <Col>
            <h4 className="text-center mt-5">Lupa Password</h4>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Kode Otp</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                type="text"
                placeholder="Ketikan kode OTP dari Email Anda"
                name="otp"
              />
            </Form.Group>

            <Button onClick={this.handleSubmit} variant="primary">
              Submit
            </Button>
            <br />
            <Link to="/login">Kembali ke halaman login</Link><br/>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

export default InsertToken;
