import React from 'react';
import FeatherIcon from 'feather-icons-react';
import { Container,CardDeck,Card,Row,Col } from 'react-bootstrap';
import axios from "axios";
class LaporanLayanan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listdata: [],
      loading: true,
    };
  }
  async componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_SERVER_URL}/infopublic/info_public/4`)
      .then((res) => {
        this.setState({ listdata: res.data.data });
        this.setState({ loading: false });
      });
  }
  render() {
    return (
      <Container fluid>
      <Row className="justify-content-md-center">
        <Col xs={10}>
          <h3 className="text-center">
            Laporan Layanan Informasi Publik
          </h3>
          <ul>
            {this.state.listdata.map((datasub, keysub) => {
              
              if(datasub.urutan_abjad=='B')
              {
                return (
                  <li className="title-sub" key={keysub}>
                   {datasub.title}
                    <ol>
                      {datasub.item.map((dataitem, keyitem) => (
                        <li className="title-item" key={keyitem}>
                          <a
                            target="_blank"
                            href={
                              dataitem.file
                                ? `${process.env.REACT_APP_SERVER_URL}../../assets/upload/infopublic/${dataitem.file}`
                                : dataitem.link
                            }
                          >
                            {dataitem.title}
                          </a>
                        </li>
                      ))}
                    </ol>
                  </li>
                );
              }
            
            })}
          </ul>
        </Col>
      </Row>
    </Container>
    );
  }
}

export default LaporanLayanan;
