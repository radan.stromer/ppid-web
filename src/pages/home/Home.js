import React from "react";
import CarouselHome from "../../components/carousel/CarouselHome";
import { Container, CardDeck, Card } from "react-bootstrap";
import "./home.css";
import {
  ArticleSample,
  InfoBerkalaHome,
  InfoSertaMertaHome,
  InfoSetiapSaatHome,
} from "../../assets";
import { Link } from "react-router-dom";
import axios from "axios";
class Home extends React.Component {
  state = {
    listArticle: null,
    loading: false,
  };
  componentDidMount() {
    this.get_list_news();
  }

  get_list_news = () => {
    axios
      .get(`https://www.bp2mi.go.id/pia2.php`)
      .then((response) => {
        // this.setState({ listpengajuan: response.data.pengajuan });
        // this.setState({ loading: false });
        this.setState({ listArticle: response.data.data.slice(0, 4) });
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <>
        <Container>
          <div className="row">
            <div className="col-sm-12 mt-2">
              <CarouselHome />
            </div>
          </div>
        </Container>
        <Container>
          <div className="row mt-5" id="section-informasi-publik">
            <div className="col-sm-4 col-xs-12 text-center">
              <Link to="/berkala">
                <img
                  alt="info-berkala"
                  className="img img-fluid"
                  src={InfoBerkalaHome}
                />
                <h5>
                  <b>Informasi berkala</b>
                </h5>
              </Link>
            </div>
            <div className="col-sm-4 col-xs-12 text-center">
              <Link to="/serta-merta">
                <img
                  alt="serta-merta"
                  className="img img-fluid"
                  src={InfoSertaMertaHome}
                />
                <h5>
                  <b>Informasi serta merta</b>
                </h5>
              </Link>
            </div>
            <div className="col-sm-4 col-xs-12 text-center">
              <Link to="/setiap-saat">
                <img
                  alt="setiap-saat"
                  className="img img-fluid"
                  src={InfoSetiapSaatHome}
                />
                <h5>
                  <b>Informasi tersedia setiap saat</b>
                </h5>
              </Link>
            </div>
          </div>

          <div id="container-judul-section">
            <div className="box-tab-title">
              <h3 className="box-tab-title">BERITA BP2MI</h3>
            </div>
          </div>
          <CardDeck className="mt-5">
            {this.state.listArticle &&
              this.state.listArticle.map((data, key) => {
                return (
                  <Card>
                    <a href={`https://bp2mi.go.id/berita-detail/${data.slug}`}>
                      <Card.Img
                        className="imgNews"
                        variant="top"
                        src={`https://www.bp2mi.go.id/${data.img}`}
                      />
                    </a>
                    <Card.Body className="cardBodyNews">
                      <a
                        style={{ color: "black " }}
                        href={`https://bp2mi.go.id/berita-detail/${data.slug}`}
                      >
                        <Card.Title>{data.judul}</Card.Title>
                      </a>
                      <Card.Text style={{ fontSize: "14px" }}>
                        {data.berita
                          .substring(0, 350)
                          .replace(/(<([^>]+)>)/gi, "")}
                      </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                      <a
                        href={`https://bp2mi.go.id/berita-detail/${data.slug}`}
                      >
                        <small className="text-muted">Selengkapnya</small>
                      </a>
                    </Card.Footer>
                  </Card>
                );
              })}
            {/* <Card>
              <Card.Img variant="top" src={ArticleSample} />
              <Card.Body>
                <Card.Title>Card title</Card.Title>
                <Card.Text>
                  This card has supporting text below as a natural lead-in to
                  additional content.{" "}
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">Selengkapnya</small>
              </Card.Footer>
            </Card>
            <Card>
              <Card.Img variant="top" src={ArticleSample} />
              <Card.Body>
                <Card.Title>Card title</Card.Title>
                <Card.Text>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This card has even longer
                  content than the first to show that equal height action.
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">Selengkapnya</small>
              </Card.Footer>
            </Card>
            <Card>
              <Card.Img variant="top" src={ArticleSample} />
              <Card.Body>
                <Card.Title>Card title</Card.Title>
                <Card.Text>
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content. This card has even longer
                  content than the first to show that equal height action.
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">Selengkapnya</small>
              </Card.Footer>
            </Card> */}
          </CardDeck>
        </Container>
      </>
    );
  }
}

export default Home;
