import React, { Component } from "react";
import { Container, Row, Col, Table, Button } from "react-bootstrap";
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios'
import { getToken } from '../../utils/Common';

export class Dashboard extends Component {
  state = {
    user: '',
    listpengajuan: null,
    loading: true,
    token: ''
  }

  get_list_pengajuan = async () => {
    const session = getToken()
    let jwtConfig = {
      headers: {
        Authorization: getToken()
      }
    }


    if (session) {
      axios.get(process.env.REACT_APP_SERVER_URL + 'pengajuan/pengajuan', jwtConfig)
        .then((response) => {
          if (response.status == 200) {
            this.setState({ listpengajuan: response.data.pengajuan });
            this.setState({ loading: false });
            console.log(response.data.pengajuan);
          }
        })
        .catch((error) => {
          console.log(error);
        });

    }
  }

  componentDidMount() {
    this.get_list_pengajuan();
  }

  render() {
    if (this.state.loading) {
      return (
        <h1>Sedang memuat....</h1>
      );
    }
    return (
      <Container fluid>
        <Row>
          <Col>
            <h4 className="text-center mt-5">List Pengajuan</h4>
            <Button className="mb-3" variant="primary" href="/tambah-pengajuan">Buat Pengajuan Baru</Button>
            <Table striped bordered hover responsive>
              <thead>
                <tr>
                  <td>Tanggal Pengajuan</td>
                  <td>Permohonan Informasi</td>
                  <td>No. Pengajuan</td>
                  <td>Cara Peroleh Info</td>
                  <td>Cara Dapat Salinan</td>
                  <td>Status</td>
                </tr>
              </thead>
              <tbody>
                {this.state.listpengajuan && this.state.listpengajuan.map((data, key) => {
                  return (
                    <tr>
                      <td>{data.waktu}</td>
                      <td>{data.rincian}</td>
                      <td>{data.no_pengajuan}</td>
                      <td>{data.cara_peroleh_info}</td>
                      <td>{data.cara_dapat_salinan}</td>
                      <td>
                        {data.status}
                        {
                          data.status == 'Sudah dijawab' || data.status == 'Keberatan sudah dijawab' ?
                            [<><br /><Button size="sm" variant="primary" href={`/lihat-detail/${data.id}`}>Lihat Jawaban</Button></>] : null
                        }
                      </td>
                    </tr>
                  )
                })
                }
              </tbody>
            </Table>
          </Col>
        </Row>

        {/* <Row>
          <Col>
            <h4 className="text-center mt-5">List Keberatan</h4>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>Tanggal Pengajuan</td>
                  <td>Permohonan Informasi</td>
                  <td>No. Pengajuan</td>
                  <td>Cara Peroleh Info</td>
                  <td>Cara Dapat Salinan</td>
                  <td>Status</td>
                </tr>
                  
              </thead>
              <tbody>
                {this.state.listpengajuan && this.state.listpengajuan.map((data,key) => {
                      return (
                        <tr>
                          <td>{data.waktu}</td>
                          <td>{data.rincian}</td>
                          <td>{data.no_pengajuan}</td>
                          <td>{data.cara_peroleh_info}</td>
                          <td>{data.cara_dapat_salinan}</td>
                          <td>{data.status}</td>
                        </tr>
                      )
                  })
                }
              </tbody>
            </Table>
          </Col>
        </Row> */}
      </Container>
    );
  }
}

export default Dashboard;
