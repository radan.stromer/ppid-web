import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect,useParams } from "react-router-dom";
import axios from "axios";
import { getToken } from "../../utils/Common";
import { formulirKeberatan, formulirPengajuan } from "../../assets";

function KeberatanSudah(props){
  return (
    <Container fluid>
        <Row className="justify-content-md-center">
          <Col sm="6">
    <h4 className="text-center mt-5">Permohonan Informasi</h4>
            <Form.Group controlId="forRincian">
              <label>Rincian Informasi</label>
              <Form.Control
                as="textarea"
                rows="3"
                value={props.pengajuan.rincian}
                name="rincian"
                readOnly
              />
            </Form.Group>

            <Form.Group controlId="forTujuan">
              <label>Tujuan Informasi</label>
              <Form.Control
                as="textarea"
                rows="3"
                
                value={props.pengajuan.tujuan}
                name="tujuan"
                readOnly
              />
            </Form.Group>

            <Row>
            <Form.Group controlId="exampleForm.ControlSelect1" className="col-sm-6">
              <Form.Label>Cara Memperoleh Informasi</Form.Label>
              <Form.Control
                
                value={props.pengajuan.cara_peroleh_info}
                name="cara_peroleh_info"
                readOnly
              />
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlSelect1" className="col-sm-6">
              <Form.Label>Cara mendapatkan salinan</Form.Label>
              <Form.Control
                
                value={props.pengajuan.cara_dapat_salinan}
                name="cara_dapat_salinan"
                readOnly
              />
            </Form.Group>
            </Row>
            <hr/>
            <Form.Group controlId="forRincian">
              <strong className="text-center">Jawaban Pengajuan</strong>
              <Form.Control
                as="textarea"
                rows="3"
                value={props.pengajuan.jawaban}
                name="rincian"
                readOnly
              />
            </Form.Group>
            <br />
            
            <br />
            <br />
            <strong className="text-center">Ajukan Keberatan</strong>
            <hr/>
            <Form.Group controlId="exampleForm.ControlSelect1">
              <label>Alasan Keberatan</label>
              <Form.Control as="select" name="alasan_keberatan" readOnly>
              <option>{props.pengajuan.alasan_keberatan}</option>
              
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="keberatan">
              <label>Isi Keberatan</label>
              <Form.Control
                as="textarea"
                rows="3"
                value={props.pengajuan.keberatan}
                placeholder="Isi Keberatan"
                name="keberatan"
                readOnly
              />
            </Form.Group>
            

            <Form.Group controlId="alasan_keberatan">
            <label>Alasan Keberatan</label>
              <Form.Control
                as="textarea"
                rows="3"
                value={props.pengajuan.info_keberatan}
                placeholder="Alasan Keberatan"
                name="info_keberatan"
                readOnly
              />
            </Form.Group>

            <Form.Group controlId="alasan_keberatan">
            <label>Jawaban Keberatan</label>
              <Form.Control
                as="textarea"
                rows="3"
                value={props.pengajuan.keberatan_jwb}
                placeholder="Alasan Keberatan"
                name="info_keberatan"
                readOnly
              />
            </Form.Group>

            <Button variant="primary">
              Selesai
            </Button>
          </Col>
        </Row>
      </Container>
        );
}

export class PengajuanDetail extends Component {
  state = {
    Pengajuan:{},
    formKeberatan :{},
    showSubmit: false,
    token: "",
    redirect: null,
    Lampiran:null
  };

  componentDidMount() {
    this.getPengajuan();
  }

  getPengajuan = () => {
    const session = getToken()
      let jwtConfig = {
        headers: {
          Authorization: getToken()
        }
    }

    axios.get(`${process.env.REACT_APP_SERVER_URL}pengajuan/pengajuan/${this.props.match.params.idpengajuan}`,jwtConfig)
    .then((res)=>{
      this.setState({
        Pengajuan : res.data.pengajuan
       })
       let lampiran = this.state.Pengajuan.link_lampiran.split(",")
       this.setState({ Lampiran: lampiran });
       //console.log('test', lampiran)
    }).then(()=>{
      console.log(this.state.Pengajuan);
    })
    
  } 

  handleFormChange = (e) => {
    let formKeberatanNew = {...this.state.formKeberatan};
    formKeberatanNew[e.target.name] = e.target.value;
    this.setState({
      formKeberatan : formKeberatanNew
    })
   
  } 

  handleSelesai = async ()=>{
    const session = getToken()
    fetch(`${process.env.REACT_APP_SERVER_URL}pengajuan/pengajuan`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': getToken()
      },
      body:JSON.stringify({ 
        'status' : 'Sudah selesai',
        'id'     :  this.props.match.params.idpengajuan
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      alert('Pengajuan Anda sudah selesai');
      this.setState({ redirect: "/dashboard" });
      //this.props.navigation.navigate('Dashboarduser'); 
    }).catch((error) => {
      console.error(error);
    });
    
  }

  handleSubmit = async () => {
    const session = getToken()
    let jwtConfig = {
        headers: {
          Authorization: getToken(),
        }
    }

    let formKeberatanNew = {...this.state.formKeberatan};
    formKeberatanNew['id'] = this.props.match.params.idpengajuan;
    await this.setState({
      formKeberatan : formKeberatanNew
    })

    axios.post(process.env.REACT_APP_SERVER_URL+'pengajuan/keberatan',this.state.formKeberatan,jwtConfig)
    .then((res)=>{
      this.setState({ redirect: "/dashboard" });
    })
    
  }

  render() {
    if (this.state.redirect) {
        return <Redirect to={this.state.redirect} />
      }
    if(this.state.Pengajuan.status=='Keberatan sudah dijawab'){
      return <KeberatanSudah pengajuan={this.state.Pengajuan}/>
    }
    
    return (
      <Container fluid>
        <Row className="justify-content-md-center">
          <Col sm="6">
    <h4 className="text-center mt-5">Permohonan Informasi</h4>
            <Form.Group controlId="forRincian">
              <label>Rincian Informasi</label>
              <Form.Control
                as="textarea"
                rows="3"
                value={this.state.Pengajuan.rincian}
                name="rincian"
                readOnly
              />
            </Form.Group>

            <Form.Group controlId="forTujuan">
              <label>Tujuan Informasi</label>
              <Form.Control
                as="textarea"
                rows="3"
                onChange={this.handleFormChange}
                value={this.state.Pengajuan.tujuan}
                name="tujuan"
                readOnly
              />
            </Form.Group>

            <Row>
            <Form.Group controlId="exampleForm.ControlSelect1" className="col-sm-6">
              <Form.Label>Cara Memperoleh Informasi</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                value={this.state.Pengajuan.cara_peroleh_info}
                name="cara_peroleh_info"
                readOnly
              />
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlSelect1" className="col-sm-6">
              <Form.Label>Cara mendapatkan salinan</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                value={this.state.Pengajuan.cara_dapat_salinan}
                name="cara_dapat_salinan"
                readOnly
              />
            </Form.Group>
            </Row>
            <hr/>
            <Form.Group controlId="forRincian">
              <strong className="text-center">Jawaban Pengajuan</strong>
              <Form.Control
                as="textarea"
                rows="3"
                value={this.state.Pengajuan.jawaban}
                name="rincian"
                readOnly
              />
            </Form.Group>
            <Form.Group controlId="forRincian">
              <strong className="text-center">Dokumen Lampiran <br/></strong>
              {
                this.state.Lampiran && this.state.Lampiran.map((v,k)=>{
                  return  <><a href={v}> Download Dokumen {k+1}</a><br/></>
                })
              }
              {/* <a href={this.state.Pengajuan.link_lampiran}> Download Dokumen</a> */}
            </Form.Group>
            <br />
            <Button variant="primary" onClick={this.handleSelesai}>
              Selesai
            </Button>
            <br />
            <br />
            <strong className="text-center">Ajukan Keberatan</strong><br/>
            <a target="_blank" href={formulirKeberatan}>Formulir Pernyataan Keberatan atas Permohonan Informasi</a>
            <hr/>
            <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Control as="select" name="alasan_keberatan" onChange={this.handleFormChange}>
                <option value="">Tentukan Alasan Keberatan</option>
                <option>Permohonan Informasi ditolak</option>
                <option>Informasi berkala tidak disediakan</option>
                <option>Permintaan informasi tidak ditanggapi</option>
                <option>Permintaan informasi ditanggapi tidak sebagaiman yang diminta</option>
                <option>Biaya yang dikenakan tidak wajar</option>
                <option>Informasi disampaikan melebihi jangka waktu yang ditentukan</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="keberatan">
              <Form.Control
                as="textarea"
                rows="3"
                onChange={this.handleFormChange}
                placeholder="Isi Keberatan"
                name="keberatan"
              />
            </Form.Group>

            <Form.Group controlId="alasan_keberatan">
              <Form.Control
                as="textarea"
                rows="3"
                onChange={this.handleFormChange}
                placeholder="Alasan Keberatan"
                name="info_keberatan"
              />
            </Form.Group>

            <Button onClick={this.handleSubmit} variant="primary">
              Kirim Keberatan
            </Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default PengajuanDetail;
