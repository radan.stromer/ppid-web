import React from 'react'
import {Container} from 'react-bootstrap'
import { mediaWaktuImg } from '../../../assets'

const mediaWaktu = () => {
    return (
        <Container>
           <img src={mediaWaktuImg} class="img-fluid"/>
        </Container>
    )
}

export default mediaWaktu
