import React from 'react'
import { AlurPermintaanInformasiPublik } from '../../../assets'

const mediaAlur = () => {
    return (
        <div>
            <img style={{ width: '80%', margin: '0 auto', display: 'block' }} src={AlurPermintaanInformasiPublik} />
        </div>
    )
}

export default mediaAlur
