import React from 'react'
import { Container } from 'react-bootstrap'
import { mediaBiayaImg } from '../../../assets'

const mediaBiaya = () => {
    return (
        <Container>
           <img src={mediaBiayaImg} class="img-fluid"/>
        </Container>
    )
}

export default mediaBiaya
