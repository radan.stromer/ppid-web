import React from 'react'
import {Container} from 'react-bootstrap'
import { prosedurSengketaImg } from '../../../assets'
const prosedurSengketa = () => {
    return (
        <Container>
           <img src={prosedurSengketaImg} class="img-fluid"/>
        </Container>
    )
}

export default prosedurSengketa
