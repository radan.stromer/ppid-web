import React from 'react'
import {Container} from 'react-bootstrap'
import { mediaJalurImg } from '../../../assets'
const mediaJalur = () => {
    return (
        <Container>
           <img src={mediaJalurImg} class="img-fluid"/>
        </Container>
    )
}

export default mediaJalur
