import React from 'react'
import { mediaCsImg } from '../../../assets'
import {Container} from 'react-bootstrap'
const mediaCs = () => {
    return (
        <Container>
           <img src={mediaCsImg} class="img-fluid"/>
        </Container>
    )
}

export default mediaCs
