import React from 'react'
import {Container} from 'react-bootstrap'
import { mediaMaklumatImg } from '../../../assets'
const mediaMaklumat = () => {
    return (
        <Container>
           <img src={mediaMaklumatImg} class="img-fluid"/>
        </Container>
    )
}

export default mediaMaklumat
