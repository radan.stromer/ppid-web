import React from 'react'
import {Col, Container, Row} from 'react-bootstrap'
import { formulirKeberatan, prosedurKeberatanImg } from '../../../assets'
const prosedurKeberatan = () => {
    return (
        <Container>
            <Row>
            <Col>
                <img src={prosedurKeberatanImg} class="img-fluid"/>
                <a style={{textAlign:'center',display:'block'}} target="_blank" href={formulirKeberatan}>Formulir Pernyataan Keberatan atas Permohonan Informasi</a>
           </Col>
           </Row>
        </Container>
    )
}

export default prosedurKeberatan
