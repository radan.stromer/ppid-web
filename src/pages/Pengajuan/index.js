import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { getToken } from "../../utils/Common";

export class Pengajuan extends Component {
  state = {
    formPengajuan: {
      rincian: "",
      tujuan: "",
      cara_peroleh_info: "",
      cara_dapat_salinan: "",
      from_mobile: "N",
    },
    showSubmit: false,
    token: "",
    file_ktp: null,
    file_dokumen: null,
    redirect: null,
  };

  handleFormChange = (e) => {
    let formPengajuanNew = { ...this.state.formPengajuan };
    formPengajuanNew[e.target.name] = e.target.value;
    this.setState({
      formPengajuan: formPengajuanNew,
    });
    console.log(this.state);
  };

  onFileChange = (event) => {
    this.setState({
      [event.target.name]: event.target.files[0],
    });
  };

  handleSubmit = async () => {
    const session = getToken();
    let jwtConfig = {
      headers: {
        Authorization: getToken(),
      },
    };
    const formData = new FormData();
    formData.append("file_ktp", this.state.file_ktp);
    formData.append("file_dokumen", this.state.file_dokumen);

    axios
      .post(
        process.env.REACT_APP_SERVER_URL + "pengajuan/submit_from_web",
        this.state.formPengajuan,
        jwtConfig
      )
      .then((res) => {
        formData.append("id_pengajuan", res.data.id_pengajuan);
        axios.post(
          process.env.REACT_APP_SERVER_URL + "pengajuan/upload_from_web",
          formData,
          jwtConfig
        );

        this.setState({ redirect: "/dashboard" });
      });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <Container fluid>
        <Row className="justify-content-md-center">
          <Col sm="6">
            <h4 className="text-center mt-5">Permohonan Informasi</h4>
            <Form.Group controlId="forRincian">
              <Form.Control
                as="textarea"
                rows="3"
                onChange={this.handleFormChange}
                placeholder="Rincian Informasi yang dibutuhkan"
                name="rincian"
              />
            </Form.Group>

            <Form.Group controlId="forTujuan">
              <Form.Control
                as="textarea"
                rows="3"
                onChange={this.handleFormChange}
                placeholder="Tujuan Memperoleh Informasi"
                name="tujuan"
              />
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Label>Cara Memperoleh Informasi</Form.Label>
              <Form.Control
                as="select"
                name="cara_peroleh_info"
                onChange={this.handleFormChange}
              >
                <option value="">Tentukan Cara peroleh informasi</option>
                <option>Melihat</option>
                <option>Membaca</option>
                <option>Mencatat</option>
                <option>Mendengarkan</option>
                <option>Mendapatkan Salinan Informasi (Soft/Hard Copy)</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Label>Cara mendapatkan salinan</Form.Label>
              <Form.Control
                as="select"
                name="cara_dapat_salinan"
                onChange={this.handleFormChange}
              >
                <option value="">Tentukan Cara dapatkan salinan</option>
                <option>Mengambil langsung</option>
                <option>Kurir Pos</option>
                <option>Pos</option>
                <option>Faksimili</option>
                <option>Email</option>
                <option>Aplikasi</option>
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.File
                id="fileKTP"
                label="Upload KTP"
                name="file_ktp"
                onChange={this.onFileChange}
              />
            </Form.Group>

            <Form.Group>
              <Form.File
                id="fileDokumen"
                label="Upload Dokumen Pendukung"
                name="file_dokumen"
                onChange={this.onFileChange}
              />
            </Form.Group>

            <Button onClick={this.handleSubmit} variant="primary">
              Kirim
            </Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Pengajuan;
