import React from "react";
import FeatherIcon from "feather-icons-react";
import { Container, CardDeck, Card, Row, Col } from "react-bootstrap";
import "./Regulasi.css";
import { regulasi1, regulasi2, regulasi3 } from "../../assets";
import axios from "axios";
class Regulasi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listdata: [],
      loading: true,
    };
  }
  async componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_SERVER_URL}/infopublic/info_public/5`)
      .then((res) => {
        console.log(res.data.data);
        this.setState({ listdata: res.data.data });
        this.setState({ loading: false });
      });
  }
  render() {
    return (
      <Container fluid>
        <Row className="justify-content-md-center">
          <Col xs={10}>
            <h5>Peraturan Mengenai Keterbukaan Informasi Publik.</h5>
            <ul>
              {this.state.listdata.map((datasub, keysub) => (
                <li className="title-sub" key={keysub}>
                  {datasub.urutan_abjad}. {datasub.title}
                  <ol>
                    {datasub.item.map((dataitem, keyitem) => (
                      <li className="title-item" key={keyitem}>
                        <a
                          target="_blank"
                          href={
                            dataitem.file
                              ? `${process.env.REACT_APP_SERVER_URL}../../assets/upload/infopublic/${dataitem.file}`
                              : dataitem.link
                          }
                        >
                          {dataitem.title}
                        </a>
                      </li>
                    ))}
                  </ol>
                </li>
              ))}
            </ul>

            <p>&nbsp;</p>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Regulasi;
