import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import axios from 'axios'
import { Redirect } from "react-router-dom"

export class Register extends Component {
  state = {
    formCustomer:{
      type : 'Pribadi',
      email : '',
      password : '',
      nama:'',
      nik:'',
      telp:'',
      alamat:'',
      pekerjaan : '',
    },
    redirect: null
  }

  handleFormChange = (e) => {
      let formCustomerNew = {...this.state.formCustomer};
      formCustomerNew[e.target.name] = e.target.value;
      this.setState({
          formCustomer : formCustomerNew
      })
  } 

  handleSubmit = () => {
    axios.post(`${process.env.REACT_APP_SERVER_URL}signup/signup`,this.state.formCustomer)
    .then((res)=>{
        if(res.data.status === '200'){
          alert(res.data.msg);
          this.setState({ redirect: "/login" });
        }else{
          alert(res.data.msg);
        }
        console.log(res.data);
    })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    return (
      <Container fluid>
        <Row>
          <Col>
            <h4 className="text-center mt-5">Daftar</h4>
              <Form.Group>
                <Form.Label>Nama</Form.Label>
                <Form.Control onChange={this.handleFormChange} type="text" placeholder="Ketikan Nama lengkap" name="nama"/>
              </Form.Group>

              <Form.Group>
                <Form.Label>Kelompok Pemohon</Form.Label>
                <Form.Control as="select" custom name="type" onChange={this.handleFormChange}>
                  <option value="Pribadi">Pribadi</option>
                  <option value="Lembaga">Lembaga</option>
                </Form.Control>
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>No KTP</Form.Label>
                <Form.Control onChange={this.handleFormChange} type="text" placeholder="Ketikan No KTP" name="nik"/>
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Telp</Form.Label>
                <Form.Control onChange={this.handleFormChange} type="text" placeholder="Ketikan No Telp" name="telp"/>
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Pekerjaan</Form.Label>
                <Form.Control onChange={this.handleFormChange} type="text" placeholder="Ketikan pekerjaan Anda" name="pekerjaan"/>
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control onChange={this.handleFormChange} type="email" placeholder="Ketikan Email Anda" name="email"/>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control onChange={this.handleFormChange} type="password" placeholder="Password" name="password"/>
              </Form.Group>
              
              <Button onClick={this.handleSubmit} variant="primary">
                Daftar
              </Button>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

export default Register;
