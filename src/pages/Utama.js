import React from "react";
import { Route } from "react-router-dom";

import Home from "./home/Home";
import Profil from "./profil/Profil";
import Tusi from "./profil/Tusi/Tusi";
import Struktur from "./profil/struktur/Struktur";
import VisiMisi from "./profil/VisiMisi/VisiMisi";
import Kontak from "./profil/Kontak/Kontak";
import Regulasi from "./Regulasi/Regulasi";

import Berkala from "./InfoPublik/Berkala/Berkala";
import SertaMerta from "./InfoPublik/SertaMerta/SertaMerta";
import SetiapSaat from "./InfoPublik/SetiapSaat/SetiapSaat";
import Video from "./InfoPublik/Video/Video";

import laporanAkses from "./Laporan/laporanAkses/LaporanAkses";
import laporanLayanan from "./Laporan/laporanLayanan/laporanLayanan";
import laporanSurvei from "./Laporan/laporanSurvei/laporanSurvei";
import Register from "./Register/";
import Login from "./Login/";
import Logout from "./logout/";
import Dashboard from "./Dashboard/";
import Pengajuan from "./Pengajuan/";
import PengajuanDetail from "./PengajuanDetail/";
import LupaPass from "./LupaPass/";

import mediaAlur from "./Media/Alur/";
import mediaJalur from "./Media/Jalur/";
import mediaWaktu from "./Media/Waktu/";
import mediaBiaya from "./Media/Biaya/";
import mediaCs from "./Media/Cs/";
import mediaMaklumat from "./Media/Maklumat/";
import Blog from "./Blog";
import InsertToken from "./LupaPass/insertToken";
import setPassword from "./LupaPass/setPassword";
import prosedurKeberatan from "./Media/ProsedurKeberatan";
import prosedurSengketa from "./Media/ProsedurSengketa";

class Utama extends React.Component {
  render() {
    return (
      <div id="main-section">
        <Route exact path="/" component={Home} />
        <Route path="/home" component={Home} />
        <Route path="/profil-sejarah" component={Profil} />
        <Route path="/tusi" component={Tusi} />
        <Route path="/profil-struktur" component={Struktur} />
        <Route path="/profil-visi-misi" component={VisiMisi} />
        <Route path="/profil-kontak" component={Kontak} />
        <Route path="/regulasi" component={Regulasi} />
        <Route path="/berkala" component={Berkala} />
        <Route path="/serta-merta" component={SertaMerta} />
        <Route path="/video" component={Video} />
        <Route path="/setiap-saat" component={SetiapSaat} />
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Logout} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/blog" component={Blog} />
        <Route path="/tambah-pengajuan" component={Pengajuan} />
        <Route path="/lihat-detail/:idpengajuan" component={PengajuanDetail} />
        <Route path="/lupa-password" component={LupaPass} />
        <Route path="/kode-otp" component={InsertToken} />
        <Route path="/ubah-password" component={setPassword} />

        {/* /* Laporan */}
        <Route path="/laporan-akses" component={laporanAkses} />
        <Route path="/laporan-layanan" component={laporanLayanan} />
        <Route path="/laporan-survei" component={laporanSurvei} />

        {/* /* Media Layanan */}
        <Route path="/media-alur" component={mediaAlur} />
        <Route path="/media-jalur" component={mediaJalur} />
        <Route path="/media-waktu" component={mediaWaktu} />
        <Route path="/media-biaya" component={mediaBiaya} />
        <Route path="/media-cs" component={mediaCs} />
        <Route path="/media-maklumat" component={mediaMaklumat} />
        <Route path="/prosedur-keberatan" component={prosedurKeberatan} />
        <Route path="/prosedur-sengketa" component={prosedurSengketa} />
      </div>
    );
  }
}

export default Utama;
