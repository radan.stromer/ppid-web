import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import axios from "axios";
class VisiMisi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      loading: true,
      contentHtml: null,
      contentHtmlRender: null
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_SERVER_URL}page/page/2`)
      .then((res) => {
        this.setState({ data: res.data.data });
        this.setState({ loading: false });
        this.setState({ contentHtml: this.renderHTML(res.data.data.description) });
      });
  }

  renderHTML() {
    return <div dangerouslySetInnerHTML={{ __html: this.state.data.description }} />
  }

  render() {
    return (
      <Container fluid>
        <Row className="justify-content-md-center">
          <Col xs={10}>
            {
              this.state.contentHtml && (<>
                <p>
                  {this.state.contentHtml}
                </p>
              </>
              )}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default VisiMisi;
