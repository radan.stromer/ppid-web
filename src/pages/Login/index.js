import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { setUserSession } from "../../utils/Common";
import { formulirPengajuan } from "../../assets";

export class Login extends Component {
  state = {
    formCustomer: {
      email: "",
      password: "",
    },
    redirect: null,
  };

  handleFormChange = (e) => {
    let formCustomerNew = { ...this.state.formCustomer };
    formCustomerNew[e.target.name] = e.target.value;
    this.setState({
      formCustomer: formCustomerNew,
    });
  };

  handleSubmit = () => {
    axios
      .post(
        process.env.REACT_APP_SERVER_URL + "login/login",
        this.state.formCustomer
      )
      .then((res) => {
        if (res.data.status == "200") {
          setUserSession(res.data.token);
          this.setState({ redirect: "/dashboard" });
        } else {
          console.log('test', res)
          //alert(res.data.status);
        }
        console.log(res.data.status);
      });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <Container fluid>
        <Row>
          <Col>
            <h4 className="text-center mt-5">Daftar</h4>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                type="email"
                placeholder="Ketikan Email Anda"
                name="email"
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                onChange={this.handleFormChange}
                type="password"
                placeholder="Password"
                name="password"
              />
            </Form.Group>

            <Button onClick={this.handleSubmit} variant="primary">
              Login
            </Button>
            <br />
            <Link to="/register">Belum punya akun? Silahkan daftar</Link><br />
            <Link to="/lupa-password">Lupa Password</Link><br />
            <a target="_blank" href={formulirPengajuan}>Formulir Permohonan informasi</a>

          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

export default Login;
