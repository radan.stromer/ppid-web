import React from 'react';
import FeatherIcon from 'feather-icons-react';
import { Container,CardDeck,Card,Row,Col } from 'react-bootstrap';
import './SertaMerta.css'
import axios from 'axios';
class SertaMerta extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listdata : [],
      loading : true
    }
  }
  async componentDidMount() {
    axios
        .get(`${process.env.REACT_APP_SERVER_URL}/infopublic/info_public/2`)
        .then((res)=>{
          console.log(res.data.data)
             this.setState({listdata:res.data.data});
            this.setState({loading:false});
        });
    
  }
  render() {
    return (
        <Container fluid>
          <Row className="justify-content-md-center">
            <Col xs={10}>
              <h3 className="text-center">Informasi Publik yang Wajib Disediakan dan Diumumkan Secara Serta Merta</h3>
              <ul>
                {this.state.listdata.map((datasub,keysub) =>
                    <li className="title-sub" key={keysub}>
                        {datasub.urutan_abjad}. {datasub.title}
                        <ol>
                        {datasub.item.map((dataitem,keyitem)=>
                            <li className="title-item" key={keyitem}>
                                <a target="_blank" href={`http://adminppid.bp2mi.go.id/assets/upload/infopublic/${dataitem.file}`}>{dataitem.title}</a>
                            </li>
                        )}
                        </ol>
                    </li>
                )}
              </ul>
            </Col>
          </Row>
        </Container>
    );
  }
}

export default SertaMerta;
