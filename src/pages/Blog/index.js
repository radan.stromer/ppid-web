import React from 'react'
import { Container, CardDeck, Card, Row, Col } from "react-bootstrap";
import { ArticleSample} from "../../assets";
import "./blog.css";

const Blog = () => {
    return (
       <Container>
           <Row>
            <Col sm='8'>
                <h1 className="mt-5">Lorem ipsum dolor sit ametm</h1>
                <p>12 Mei 2020</p>
                <img className="img-fluid mb-5" src={ArticleSample} />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus blandit lacus a sodales. Pellentesque nec tellus ultrices, dictum erat sit amet, malesuada augue. Suspendisse suscipit cursus lectus, sit amet volutpat enim bibendum vel. Donec pretium eu tortor non iaculis. In diam sem, feugiat consectetur bibendum ac, maximus vitae ante. Etiam a eleifend augue. Proin consequat dui enim, eu varius tortor tempus at. Praesent cursus efficitur scelerisque. Nulla et ipsum imperdiet, ultrices ante sed, mollis massa. Morbi malesuada purus velit. Aliquam erat volutpat. Nunc ultrices porta interdum. Maecenas consectetur nibh nec interdum dapibus.</p>

                <p>Donec tincidunt arcu auctor erat congue, in blandit est rutrum. Mauris tempus ut libero ac ullamcorper. Quisque sed magna et elit efficitur luctus. Etiam facilisis id mi sit amet convallis. Etiam congue condimentum lectus et eleifend. Nulla ultrices rhoncus ante, sed malesuada felis. Maecenas nec volutpat sapien. Morbi pharetra sollicitudin felis, eget maximus purus volutpat sed.
                </p>
                <p>
                     In aliquet elementum nulla, in eleifend sem varius quis. Aliquam sit amet tincidunt libero. Mauris posuere urna et nisl luctus, vitae elementum nulla efficitur. Nam fringilla pharetra nisi, nec tincidunt velit auctor eget. Nulla arcu nulla, aliquet nec egestas a, blandit at tellus. Maecenas cursus semper orci, sed molestie metus pellentesque ut. Praesent consequat libero in ex consectetur ultrices. Vivamus dui ligula, sagittis non consectetur at, pulvinar vel leo. In hac habitasse platea dictumst. Duis placerat, nibh ut volutpat commodo, ipsum enim commodo urna, id efficitur augue massa dictum leo. Sed scelerisque dignissim odio et ornare. Integer eu lacus egestas, bibendum justo a, euismod arcu.


                </p>
            </Col>
            <Col sm='4'>
                <h3 className="mt-5">Berita terkait</h3>
                <Row>
                    <Col xs='6'>
                        <img className="img-fluid mb-5" src={ArticleSample} />
                    </Col>
                    <Col xs='6'>
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p>22 Mei 2020</p>
                    </Col>
                </Row>
                <Row>
                    <Col xs='6'>
                        <img className="img-fluid mb-5" src={ArticleSample} />
                    </Col>
                    <Col xs='6'>
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p>22 Mei 2020</p>
                    </Col>
                </Row>
                <Row>
                    <Col xs='6'>
                        <img className="img-fluid mb-5" src={ArticleSample} />
                    </Col>
                    <Col xs='6'>
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p>22 Mei 2020</p>
                    </Col>
                </Row>
                <Row>
                    <Col xs='6'>
                        <img className="img-fluid mb-5" src={ArticleSample} />
                    </Col>
                    <Col xs='6'>
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p>22 Mei 2020</p>
                    </Col>
                </Row>
                <Row>
                    <Col xs='6'>
                        <img className="img-fluid mb-5" src={ArticleSample} />
                    </Col>
                    <Col xs='6'>
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p>22 Mei 2020</p>
                    </Col>
                </Row>
            </Col>
           </Row>
       </Container>
    )
}

export default Blog
