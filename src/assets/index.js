import mediaAlurImg from './images/Alur.jpg'
import mediaBiayaImg from './images/biayalayanan.png'

import StrukturOrg from './images/struktur-organisasi-PPID.png'
import mediaCsImg from './images/callcenter.png'
import mediaJalurImg from './images/jalur-layanan.png'
import mediaMaklumatImg from './images/maklumat.png'
import mediaWaktuImg from './images/waktulayanan.png'
import Sejarah2PageImg from './images/sejarah2.jpg'
import Sejarah1PageImg from './images/sejarah1.jpg'
import VisiMisiPageImg from './images/visi.jpeg'
import Contact from './images/Contact.png'

import LogoHeader from './images/PPID__2_-removebg-preview.png'
import LogoFooter from './images/logo-footer.png'

import InfoBerkalaHome from './images/info-berkala.png'
import InfoSertaMertaHome from './images/info-sertamerta.png'
import InfoSetiapSaatHome from './images/info-tersediasetiapsaat.png'

import Slide1 from './images/carousel/1.png'
import Slide2 from './images/carousel/2.jpeg'
import Slide3 from './images/carousel/3.jpeg'

import ArticleSample from './images/mPPID.png'

import regulasi3 from './pdf/PERKA_No_13_Tahun_2014_Tentang_Pedoman_Kerja_Pejabat_Pengelola_Informasi_dan_Dokumentasi_di_Lingkungan_BNP2TKI.pdf'
import regulasi2 from './pdf/UU Nomor 18 Tahun 2017.pdf'
import regulasi1 from './pdf/UU_No_14_Tahun_2008.pdf'
import formulirPengajuan from './pdf/Formulir Permohonan Informasi Publik.pdf'
import formulirKeberatan from './pdf/Formulir Keberatan.pdf'

import prosedurKeberatanImg from './images/PROSEDUR PENGAJUAN KEBERATAN.png'
import prosedurSengketaImg from './images/PROSEDUR PERMOHONAN PENYELESAIAN SENGKETA INFORMASI.png'
import AlurPermintaanInformasiPublik from './images/AlurPermintaanInformasiPublik.jpg'



export { mediaBiayaImg, mediaCsImg, mediaJalurImg, mediaMaklumatImg, mediaWaktuImg, mediaAlurImg, Sejarah2PageImg, Sejarah1PageImg, VisiMisiPageImg, LogoHeader, InfoBerkalaHome, InfoSertaMertaHome, InfoSetiapSaatHome, Slide1, Slide2, Slide3, ArticleSample, LogoFooter, StrukturOrg, Contact, regulasi1, regulasi2, regulasi3, formulirPengajuan, formulirKeberatan, prosedurKeberatanImg, prosedurSengketaImg, AlurPermintaanInformasiPublik }