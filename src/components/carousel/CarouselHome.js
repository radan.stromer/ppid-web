import React,{useEffect,useState} from 'react';
import { Carousel } from 'react-bootstrap';
import './caraouselhome.css'
import axios from 'axios'

const CarouselHome = () => {
    const [slide, setSlide] = useState()
    useEffect(()=>{
        axios
        .get(`${process.env.REACT_APP_SERVER_URL}/slide/get_slide`)
        .then((res)=>{
            setSlide(res.data.data);
        });
    },[])
    return (
        <Carousel>
            {
                slide && slide.map((data)=>{
                    return (
                        <Carousel.Item>
                            <img className="img-carousel"
                            src={`${process.env.REACT_APP_SERVER_HOST}assets/upload/slide/${data.gambar}`}
                            alt="First slide"
                            />
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
    )
}

export default CarouselHome
