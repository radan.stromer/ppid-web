import React from 'react';
import FeatherIcon from 'feather-icons-react';
import { Nav,Navbar,NavDropdown,Form,FormControl,Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Header.css';
import { getToken } from '../../utils/Common';

class NavLogin extends React.Component {
  state = {
    token :''
  }
  componentDidMount() {
      this.setState({token:getToken()});
  }
  render() {
    return (
        <>
            <Nav.Link href="/dashboard">Pengajuan</Nav.Link>
            <Nav.Link href="/logout">Logout</Nav.Link>
        </>
    );
  }
}

export default NavLogin;
