import React from 'react';
import FeatherIcon from 'feather-icons-react';
import { Nav,Navbar,NavDropdown,Form,FormControl,Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Header.css';
import { getToken } from '../../utils/Common';

class NavLogout extends React.Component {
  state = {
    token :''
  }
  componentDidMount() {
      this.setState({token:getToken()});
  }
  render() {
    return (
        <>
            <Nav.Link href="/login">Login</Nav.Link>
        </>
    );
  }
}

export default NavLogout;
