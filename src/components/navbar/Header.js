import React from "react";
import { Nav, Navbar, NavDropdown, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Header.css";
import NavLogin from "./NavLogin";
import NavLogout from "./NavLogout";
import { getToken } from "../../utils/Common";
import { LogoHeader } from "../../assets";

class Header extends React.Component {
  state = {
    token: "",
  };
  componentDidMount() {
    this.setState({ token: getToken() });
  }
  render() {
    const token = this.state.token;
    return (
      <>
        <>
          <Navbar id="navbar-ppid" expand="lg">
            <Navbar.Brand href="#home">
              <img
                style={{ width: "65%" }}
                src={LogoHeader}
                alt="First slide"
              />
            </Navbar.Brand>
          </Navbar>
        </>
        <Container fluid id="container-nav-menu">
          <Navbar id="nav-menu" expand="lg">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#">
                  <Link to="/">Beranda</Link>
                </Nav.Link>
                <NavDropdown title="Seputar PPID" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/profil-sejarah">
                    Profil Singkat
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/profil-visi-misi">
                    Visi dan Misi BP2MI
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/profil-struktur">
                    Struktur Organisasi
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/profil-kontak">
                    Lokasi & Kontak PPID
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/tusi">
                    Tugas fungsi dan wewen...
                  </NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="/regulasi">Peraturan</Nav.Link>
                <NavDropdown title="Informasi publik" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/berkala">
                    Informasi Berkala
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/serta-merta">
                    Informasi Serta Merta
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/setiap-saat">
                    Informasi Tersedia Setiap Saat
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/video">
                    Video Informasi Publik
                  </NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Laporan" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/laporan-akses">
                    Laporan Akses Informasi Publik
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/laporan-layanan">
                    Laporan Layanan Informasi Publik
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/laporan-survei">
                    Laporan Survei Informasi Publik
                  </NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Media layanan" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/media-maklumat">
                    Maklumat Pelayanan Publik
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/media-alur">
                    Alur Permohonan Informasi
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/prosedur-keberatan">
                    Prosedur Pengajuan Keberatan
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/prosedur-sengketa">
                    Prosedur Sengketa Informasi
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/media-jalur">
                    Jalur Layanan
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/media-waktu">
                    Waktu Layanan
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/media-biaya">
                    Biaya Layanan
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/media-cs">
                    Call Center
                  </NavDropdown.Item>
                </NavDropdown>
                {token ? [<NavLogin />] : [<NavLogout />]}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </Container>
      </>
    );
  }
}

export default Header;
