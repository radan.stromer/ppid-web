import React from 'react';
import FeatherIcon from 'feather-icons-react';
import { Container, Row, Col } from 'react-bootstrap';
import './Footer.css'
import { LogoFooter } from '../../assets';
import { FaTiktok } from "react-icons/fa";

class Footer extends React.Component {
    render() {
        return (
            <>
                <Container fluid>
                    <Row className="mt-5" style={{ background: '#01275C', padding: '1%' }}>
                        <Col style={{ textAlign: 'center', color: 'white' }}>
                            Peta situs | Hubungi Kami | Prasyarat | FAQ
                        </Col>
                    </Row>
                </Container>
                <Container fluid style={{ background: '#16529b', padding: '1%' }}>
                    <Row className="justify-content-center">
                        <Col sm="8">
                            <img style={{ width: '10%', float: 'left' }}
                                className=" pull-right"
                                src={LogoFooter}
                                alt="First slide"
                            />
                            <p style={{ lineHeight: 1, paddingTop: '2%', marginLeft: '2%', width: '70%', float: 'left', fontSize: '16px', color: 'white' }}>
                                Jl.MT Haryono Kav.52 Pancoran, <br />Jakarta Selatan 12095, <br />Indonesia
                            </p>
                        </Col>
                        <Col sm="4">
                            <span style={{ color: 'yellow' }}>Ikuti Kami </span><br />
                            <a target="_blank" className="navbar-footer" href="https://id-id.facebook.com/bp2mi.ri"><FeatherIcon style={{ color: 'white' }} icon="facebook" /></a>
                            <a target="_blank" className="navbar-footer" href="https://twitter.com/bp2mi_ri"><FeatherIcon style={{ color: 'white' }} icon="twitter" /></a>
                            <a target="_blank" className="navbar-footer" href="https://www.youtube.com/c/BP2MIHumas"><FeatherIcon style={{ color: 'white' }} icon="youtube" /></a>
                            <a target="_blank" className="navbar-footer" href="https://www.instagram.com/bp2mi_ri/"><FeatherIcon style={{ color: 'white' }} icon="instagram" /></a>
                            <a target="_blank" className="navbar-footer" href="https://www.tiktok.com/@bp2mi.ri"><FaTiktok style={{ color: 'white' }} /></a>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Footer;
