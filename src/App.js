import React from 'react';
import Utama from './pages/Utama';
import Header from './components/navbar/Header';
import Footer from './components/footer/Footer';
import { Link } from 'react-router-dom';
import './App.css';

class App extends React.Component {
  constructor(){
    super();
    global.apiEndPoint = 'http://adminppid.bp2mi.go.id/';
    global.apifileEndPoint = 'http://adminppid.bp2mi.go.id/';
  }
  render() {
    return (
      <>
          <Header />
      
          <Utama />
       
          <Footer />
      </>
    );
  }
}

export default App;
